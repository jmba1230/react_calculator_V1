import React from 'react';
import ReactDOM from 'react-dom';
// This is our Counter App
// import App from './App';

// This is our ForexApp
// import ForexApp from './ForexApp';

// This is for CalculatorApp
import CalculatorApp from './CalculatorApp';

// This is for SimpleCalc
// import SimpleCalc from './SimpleCalc';

import * as serviceWorker from './serviceWorker';

//bootstrap
import 'bootstrap/dist/css/bootstrap.css';

ReactDOM.render(
  <React.StrictMode>
    <CalculatorApp />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
